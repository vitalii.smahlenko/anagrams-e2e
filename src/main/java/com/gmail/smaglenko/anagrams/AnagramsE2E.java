package com.gmail.smaglenko.anagrams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AnagramsE2E {
    public static void main(String[] args) {
        try {
            String[] arguments = new String[]{"\"second       1ine\"",
                    "\"a1bcd efg!h\"",
                    "\"He11o W0r1d\""};
            //Put the absolute path to the application repository into a variable repositoryPath
            String repositoryPath = "C:\\repository\\anagrams";
            String application = String.format("%s\\dist\\anagrams.bat",
                    repositoryPath);

            ProcessBuilder processBuilder = new ProcessBuilder(
                    application, arguments[0], arguments[1],
                    arguments[2]);

            Process process = processBuilder.start();

            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String printResultOutput = "";
            while ((printResultOutput = in.readLine()) != null) {
                System.out.println(printResultOutput);
            }

            int exitValue = process.waitFor();
            if (exitValue == 0) {
                System.out.println("Success!");
                System.out.println("Exit value: " + exitValue);
                System.exit(0);
            } else {
                System.out.println("Test failed!!! Exit value: " + exitValue);
            }

        } catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}

