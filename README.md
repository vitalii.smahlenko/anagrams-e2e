**Task 2 - Anagrams testing: advanced task: implement e2e**

Write anagrams-e2e application that tests Anagrams.

It runs anagrams.jar with predefined test arguments and verifies its 

output (google: "java run external program and capture output").

It returns 0 on success and 1 if one or more tests failed.

Implement a few tests, you don't have to cover the application functionality thoroughly.